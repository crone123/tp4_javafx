import javafx.application.*;
import javafx.event.*;
import javafx.scene.*;
import javafx.scene.control.*;
import javafx.scene.layout.*;
import javafx.stage.*;
import javafx.fxml.*;
import java.net.*;
import java.io.*;

public class Main extends Application
{
	private Modele modele;
	private Controlleur controlleur;
	@Override
	public void start(Stage primaryStage) throws Exception
	{
		try
		{
			URL fxmlURL = getClass().getResource("Projet.fxml");
			FXMLLoader fxmlLoader = new FXMLLoader(fxmlURL);
			VBox root = new VBox();
			fxmlLoader.setRoot(root);
			fxmlLoader.load();
			Scene scene = new Scene((VBox) root, 640, 480);
			modele = new Modele(scene);
			controlleur = fxmlLoader.getController();
			controlleur.setModele(modele);
			//controlleur = new Controlleur(modele);			
			//primaryStage.setResizable(false);
			primaryStage.setScene(scene);
			primaryStage.setTitle("Fenêtre JavaFX");
			primaryStage.show();
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}
	public Modele getModele()
	{
		return modele;
	}
	public void setControlleur(Controlleur c)
	{
		controlleur = c;
	}
	public static void main(String[] args)
	{
		Application.launch(args);
	}
	
}
