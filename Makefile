all: Main.class Modele.class Controlleur.class
Main.class: Main.java Modele.class Controlleur.class
	javac $<
Modele.class: Modele.java
	javac $<
Controlleur.class: Controlleur.java
	javac $<
.PHONY: clean run
run: Main.class
	java Main
clean:
	rm -f *.class
