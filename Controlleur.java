import javafx.application.*;
import javafx.event.*;
import javafx.scene.*;
import javafx.scene.control.*;
import javafx.scene.layout.*;
import javafx.stage.*;
import javafx.fxml.*;
import java.net.*;
import java.io.*;

public class Controlleur
{
	private Modele modele;
	private Main main;
	public Controlleur()
	{
	
	}
	public Controlleur(Main m)
	{
		main = m;
		m.setControlleur(this);
	}
	@FXML
	public void initialize()
	{
	}
	public Controlleur(Modele m)
	{
		modele = m;
	}
	public void setModele(Modele m)
	{
		modele = m;
	}
	@FXML
	public void creerModele()
	{
		modele.reset();
		TextField nom = (TextField) modele.getScene().lookup("#textNom"), prenom = (TextField)modele.getScene().lookup("#textPrenom");
		try
		{
			modele.setNom(nom.getText());
		}
		catch (Exception e)
		{
			modele.addError(e.getMessage());
		}
		try
		{
			modele.setPrenom(prenom.getText());
		}
		catch (Exception e)
		{
			modele.addError(e.getMessage());
		}
		modele.updateView();
	}
}
