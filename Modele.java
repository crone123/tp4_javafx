import javafx.application.*;
import javafx.event.*;
import javafx.scene.*;
import javafx.scene.control.*;
import javafx.scene.layout.*;
import javafx.stage.*;
import javafx.fxml.*;
import java.net.*;
import java.io.*;


public class Modele
{
	private String nom, prenom, error;
	private Scene scene;
	public Modele(Scene s)
	{
		nom = new String();
		prenom = new String();
		error = new String();
		scene = s;
	}
	public Scene getScene()
	{
		return scene;
	}
	public void setNom(String n)
	{
		if(n.equals(""))
		{
			throw new IllegalArgumentException("Veuillez entrer un nom.");
		}
		nom = new String(n);
	}
	public void setPrenom(String p)
	{
		if(p.length() < 2)
		{
			throw new IllegalArgumentException("Veuillez entrer un prénom.");
		}
		prenom = new String(p);
	}
	public void addError(String e)
	{
		error = error + e + "\n";
	}
	public String getNom()
	{
		return nom;
	}
	public String getPrenom()
	{
		return prenom;
	}
	public String toString()
	{
		return prenom + " " + nom;
	}
	public void reset()
	{
		error = "";
		nom = "";
		prenom = "";
	}
	public void updateView()
	{
		Label labelBas = (Label) getScene().lookup("#labelBas");
		Label labelHaut = (Label) getScene().lookup("#labelHaut");
		labelHaut.setText(error);
		labelBas.setText(this.toString());
	}
}
